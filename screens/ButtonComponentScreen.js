import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Colors from '../constants/Colors';
import { Button } from 'react-native-elements';

export default function ComponentScreen() {
    return (
        <View style={styles.container}>
            <Button buttonStyle={[styles.buttonContainer, styles.buttonMain]} title="Success" onPress={() => alert("Success")} />
            <Button buttonStyle={[styles.buttonContainer, styles.buttonSecundary]} title="Mi Movistar" onPress={() => alert("Bienvenido")} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginLeft: 20,
        marginTop: 25,
    },
    buttonContainer: {
        width: 200,
        marginBottom: 25
    },
    buttonMain: {
        backgroundColor: Colors.mainButton,
    },
    buttonSecundary: {
        backgroundColor: Colors.secundaryButton,
    }
});
